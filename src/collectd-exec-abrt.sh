#!/bin/bash

HOSTNAME="${COLLECTD_HOSTNAME:-$(hostname -f)}"

INTERVAL="${COLLECTD_INTERVAL:-60}"

while sleep "$INTERVAL"
do
  #  We need to check the rpms that crashed recently, since we check every 10 min, get the reported crashes from the last 10 mins
  timestamp=$(date +%s)
  tenminutesbefore=$((timestamp - 10 * 60 * 1000))
  abrt_reports="$(/usr/bin/abrt-cli list -s ${tenminutesbefore})"
  message="$abrt_reports"
  echo "PUTNOTIF plugin=exec-abrt host=${HOSTNAME} type=testing severity=FAILURE time=$timestamp message=\"$message\""
done

exit 0
