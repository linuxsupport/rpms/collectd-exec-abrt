# collectd-exec-abrt

Plugin for detecting abrt crashes

The main purpose of this plugin is to send notifications when a node detects an RPM crash, thanks to the Puppet ABRT module

**NOTE: This is still a Work in progress, we still do no parsing at all of the information we can retrieve, for now we just dump all output to the notification, we need to test how well this prints on GNI alarms.**
