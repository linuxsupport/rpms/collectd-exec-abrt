%global libvirt_version  3.9.0

Name:           collectd-exec-abrt
Version:        1.1.0
Release:        2%{?dist}
Summary:        Simple collectd exec plugin to retrieve abrt crashes
License:        GNU General Public License v3
Group:          Development/Libraries
Packager:       Linux.Support@cern.ch
BuildArch:      noarch
Url:            https://gitlab.cern.ch/linuxsupport/rpms/collectd-exec-abrt

Source0:        %{name}-%{version}.tar.gz

Requires:       collectd
Requires:       abrt
Requires:       libvirt-client >= %{libvirt_version}
Requires:       abrt-cli
Requires:       util-linux

Requires(post):   policycoreutils, collectd
Requires(preun):  policycoreutils, collectd
Requires(postun): policycoreutils

BuildRequires:  selinux-policy
BuildRequires:  selinux-policy-devel
BuildRequires:  selinux-policy-targeted
BuildRequires:  checkpolicy

%{?systemd_requires}
BuildRequires: systemd

%description
Simple collectd exec plugin to send notifications from a node if
an abrt crash is detected

%prep
%setup -q -n %{name}-%{version}

%build

# selinux, get the Type enforcement running this command after checking the script was failing: audit2allow -M MYPOLICY < /var/log/audit/audit.log
# We know this is needed because we could see the following on /var/log/collectd.log 
# [2020-06-26 17:42:53] exec plugin: exec_read_one: error = Private Reports is enabled, use 'abrt-cli -a COMMAND' to get the detected problems.
# and with tail -100000 /var/log/audit/audit.log | audit2why we could see missing rules
%install
install -p -D -m 755 src/collectd-exec-abrt.sh %{buildroot}%{_bindir}/collectd-exec-abrt.sh

# selinux
make -f %{_datadir}/selinux/devel/Makefile collectd_exec_abrt.pp src/collectd_exec_abrt.te
install -p -m 644 -D collectd_exec_abrt.pp %{buildroot}%{_datadir}/selinux/packages/%{name}/collectd_exec_abrt.pp

%post
if [ "$1" -le "1" ] ; then # First install
semodule -i %{_datadir}/selinux/packages/%{name}/collectd_exec_abrt.pp 2>/dev/null || :
fi
%systemd_post collectd.service

%preun
if [ "$1" -lt "1" ] ; then # Final removal
semodule -r collectd_exec_abrt 2>/dev/null || :
fi
%systemd_preun collectd.service

%postun
if [ "$1" -ge "1" ] ; then # Upgrade
semodule -i %{_datadir}/selinux/packages/%{name}/collectd_exec_abrt.pp 2>/dev/null || :
fi
%systemd_postun_with_restart collectd.service

%files
%attr(0755, root, root) %{_bindir}/collectd-exec-abrt.sh
%attr(0644, root, root) %{_datadir}/selinux/packages/%{name}/collectd_exec_abrt.pp
%doc src/README.md



%changelog

* Thu Jun 25 2020 Daniel Juarez Gonzalez <djuarezg@cern.ch> - 1.0.0-2
- Initial release of the exec plugin
